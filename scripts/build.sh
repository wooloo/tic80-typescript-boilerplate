#!/bin/bash
# Produces a minified cart file
which uglifyjs >/dev/null || npm i -g uglify-js
tsc
mkdir -p intermediate
cd intermediate
uglifyjs --compress top_retain=["TIC","SCN","OVR"] -m reserved=["TIC","SCN","OVR"] --mangle-props --toplevel -o game.js -b -- game_big.js
csplit ../outside.js '/\/\/ SPLIT!/' >/dev/null
mkdir -p ../out
cat xx00 game.js xx01 >../out/game.js
cd ../
