#!/bin/bash
# Produces executables for various platforms
bash ./scripts/build.sh
tic80 --fs './out' --skip --cmd 'LOAD game.js & SAVE game.tic & exit' &
TARGETS="win linux rpi mac html"
for i in $TARGETS; do
	tic80 --fs './out' --skip --cmd "LOAD game.js & EXPORT $i game-$i & exit" &
done
wait
echo "Done!"
