# TIC-80 TypeScript boilerplate

This repository contains a boilerplate for using TypeScript with TIC-80.

* You should edit [outside.js](outside.js) using TIC-80's provided graphics and sound tools.
* You should edit [game.ts](game.ts) with your text editor of choice.
* Before building, run `npm i` in the repository root.
